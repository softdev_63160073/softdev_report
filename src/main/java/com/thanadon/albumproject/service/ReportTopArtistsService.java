/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.albumproject.service;

import com.thanadon.albumproject.dao.TopArtistsDao;
import com.thanadon.albumproject.model.ReportTopArtists;
import java.util.List;

/**
 *
 * @author Acer
 */
public class ReportTopArtistsService {
    public  List<ReportTopArtists> getReportTopAllTime(){
        TopArtistsDao dao = new TopArtistsDao();
        return dao.getTopAllTime();
    }
    public  List<ReportTopArtists> getReportTopYear(int year){
        TopArtistsDao dao = new TopArtistsDao();
        return dao.getTopYear(year);
    }
}
