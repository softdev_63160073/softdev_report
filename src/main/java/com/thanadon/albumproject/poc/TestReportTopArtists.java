/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.albumproject.poc;

import com.thanadon.albumproject.model.ReportTopArtists;
import com.thanadon.albumproject.service.ReportTopArtistsService;
import java.util.List;

/**
 *
 * @author Acer
 */
public class TestReportTopArtists {
    public static void main(String[] args) {
        ReportTopArtistsService reportTopArtists = new ReportTopArtistsService();
        //List<ReportTopArtists> report = reportTopArtists.getReportTopAllTime();
        List<ReportTopArtists> report = reportTopArtists.getReportTopYear(2013);
        for(ReportTopArtists r:report){
            System.out.println(r);
        }
    }
}
