/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.albumproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class ReportTopArtists {
    String name;
    String period;
    double total;

    public ReportTopArtists(String name, String period, double total) {
        this.name = name;
        this.period = period;
        this.total = total;
    }

    public ReportTopArtists() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReportTopArtists{" + "name=" + name + ", period=" + period + ", total=" + total + '}';
    }
    
    public static ReportTopArtists fromRS(ResultSet rs) {
        ReportTopArtists obj = new ReportTopArtists();
        try {
            obj.setName(rs.getString("Name"));
            obj.setPeriod(rs.getString("year"));
            obj.setTotal(rs.getDouble("total"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportTopArtists.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
