/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.albumproject.dao;

import com.thanadon.albumproject.helper.DatabaseHelper;
import com.thanadon.albumproject.model.ReportTopArtists;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class TopArtistsDao {

    public List<ReportTopArtists> getTopAllTime() {
        ArrayList<ReportTopArtists> list = new ArrayList();
        String sql = "SELECT Name,\"All Time\" as year, SUM(Quantity*UnitPrice) as total FROM track_sale\n"
                + "GROUP BY ArtistId, Name\n"
                + "ORDER BY total DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportTopArtists item = ReportTopArtists.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportTopArtists> getTopYear(int year) {
        ArrayList<ReportTopArtists> list = new ArrayList();
        String sql = "SELECT Name,strftime(\"%Y\", InvoiceDate) as year, SUM(Quantity*UnitPrice) as total FROM track_sale\n"
                + "WHERE strftime(\"%Y\", InvoiceDate) = \""+year+"\"\n"
                + "GROUP BY ArtistId, Name\n"
                + "ORDER BY total DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportTopArtists item = ReportTopArtists.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
